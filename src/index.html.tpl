<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
    <title>Plain HTML site using GitLab Pages</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>

    <h1>Hello ${CI_COMMIT_BRANCH}${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME} !</h1>

    <p>
      This is a simple plain-HTML website on Froggit Pages, without any fancy static site generator.
    </p>
  </body>
</html>

